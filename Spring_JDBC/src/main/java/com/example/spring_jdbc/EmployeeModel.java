package com.example.spring_jdbc;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Value;

@Data
@Entity
@Table(name = "employee_details")
public class EmployeeModel {
	@Id
	@Column(name = "emp_no")
	private String emp_no;
	@Column(name= "emp_name")
	private String emp_name;
	@Column(name= "age")
	private int age;
	@Column(name= "dob")
	private String dob;
	@Column(name= "project_name")
	private String project_name;
	@Column(name= "salary")
	private float salary;
	@Column(name= "years_of_experience")
	private int years_of_expereince;
	@Column(name= "employer_name")
	private String employer_name;
}
