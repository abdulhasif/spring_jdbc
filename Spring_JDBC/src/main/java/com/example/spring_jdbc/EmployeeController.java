package com.example.spring_jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@CrossOrigin(origins = "http://localhost:8080")
	@GetMapping("/employee")
	public @ResponseBody Iterable<EmployeeModel> getData()
	{
		//System.out.print(employeeRepository);
		return employeeRepository.findAll();
	}
	
	@PostMapping("/addemployee")
	public EmployeeModel addEmployee(@RequestBody EmployeeModel emp)
	{
		System.out.print("Comming");
		return  employeeRepository.save(emp);
	}
	
	@PostMapping("/updateEmployee")
	public EmployeeModel updateEmployee(@RequestBody EmployeeModel emp)
	{
		employeeRepository.delete(emp);
		return employeeRepository.save(emp);
	}
	@GetMapping("/test")
	public String Test()
	{
		return "Working";
	}
}
